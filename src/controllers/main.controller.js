const fs = require('fs').promises;
const path = require('path')

module.exports = {
  getFiles: async (req, res, next) => {
    try {
      const data = await fs.readdir(`./src/database`, 'utf8');

      console.log(`"message": "Success", "files": ${data}`);
      res.end(`"message": "Success", "files": ${data}`)
    } catch (e) {
      next(e)
    }
  },

  getFile: async (req, res, next) => {
    try {
      const { filename } = req.params
      const data = await fs.readdir(`./src/database`, 'utf8');

      let result
      for (let item of data) {
        if (filename === item) {
          result = {
            "message": "Success",
            "filename": `${filename}`,
            "content": `${await fs.readFile(`./src/database/${filename}`, 'utf8')}`,
            "extension": `${filename.split('.').pop()}`,
            "uploadedDate": "2017-07-21T17:32:28Z"
          }
        } else {
          result = {
            "message": "No file with 'notes.txt' filename found"
          }
        }
      }

      console.log(result);
      res.end(JSON.stringify(result))
    } catch (e) {
      next(e)
    }
  },

  createFile: async (req, res, next) => {
    try {

      const { filename, content } = req.body
      await fs.writeFile(`./src/database/${filename}`, JSON.stringify(`${content}`), 'utf8');

      console.log(`"message": "File created successfully"`);
      res.end(`"message": "File created successfully"`)

    } catch (e) {
      next(e)
    }
  }
}