const { Router } = require(`express`);

const {
  mainControllers: {
    createFile,
    getFiles,
    getFile
  }
} = require(`../controllers`);

const mainRouter = Router();

mainRouter.get(`/`, getFiles);
mainRouter.get(`/:filename`, getFile);
mainRouter.post(`/`, createFile);

module.exports = mainRouter;