const express = require(`express`);
const app = express();

const { mainRouter } = require(`./routers`)

app.use(express.json());

app.get('/', (req, res) => {
  res.end('Node is great!')
})

app.use(`/api/files`, mainRouter)

app.use('*', (err, req, res, next) => {
  res
    .status(err.status || 500)
    .json({
      message: err.message || 'NOT FOUND',
      code: err.customCode || 'SOMETHING WENT WRONG - SERVER ERROR'
    })
})

module.exports = app